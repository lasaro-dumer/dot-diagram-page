![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---
## Running locally

### Preparations
#### Common steps

Install nodeJS (from [NodeJS Org](https://nodejs.org/en/)).

Install Gulp:
```
npm install -g gulp-cli
```

On the root folder of the project, install the node packages:
```
npm install
```

#### Windows

Install Graphviz:
* Download Graphviz 2.44 (`.exe` available [here](https://ci.appveyor.com/api/buildjobs/92iav81dkovdrv4o/artifacts/build%2FGraphviz-install.exe)).
* Execute the installer (choose to add it to the `PATH`)
* On a terminal (PowerShell or Commmand Prompt), as Administrator, execute the command `dot -c`

### Running

On the directory folder, execute the command:
```
gulp local
```

You can now continue ediding any file on `src` and the site will refresh automatically.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
