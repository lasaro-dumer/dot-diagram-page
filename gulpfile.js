const { src, dest, series } = require('gulp');
var exec = require('child_process').exec;
const fs = require('fs-extra');
const path = require('path');
const webserver = require('gulp-webserver');
const watch = require('gulp-watch');
const clean = require('gulp-clean');
const flatten = require('gulp-flatten');

function cleanAll() {
    return src('public', {allowEmpty:true, read: false})
        .pipe(clean());
}

function buildSvg(cb){
    var dotPath = path.join('src', 'diagram.dot');
    var svgPath = path.join('public', 'diagram.svg');
    // With a callback:
    fs.ensureDir('public', err => {
        console.log(err) // => null
        // dir has now been created, including the directory it is to be placed in
    })

    exec(`dot -Tsvg ${dotPath} -o ${svgPath}`, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
      });
}

function buildHtml(cb) {
    return src('src/**/*.html')
        .pipe(dest('public'));
}

function buildCss(cb) {
    return src('node_modules/bootstrap/dist/css/*.min.css')
        .pipe(src('src/**/*.css'))
        .pipe(flatten())
        .pipe(dest('public/css'));
}

function buildJS(cb) {
    return src('node_modules/jquery-mousewheel/*.js')
        .pipe(src('src/**/*.js'))
        .pipe(src('node_modules/jquery-color/*.js'))
        .pipe(src('node_modules/popper.js/dist/*.min.js'))
        .pipe(src('node_modules/bootstrap/dist/js/*.js'))
        .pipe(src('node_modules/jquery/dist/*.min.js'))
        .pipe(flatten())
        .pipe(dest('public/js'));
}

const buildOnly = series(buildSvg, buildHtml, buildCss, buildJS);
const build = series(cleanAll, buildOnly);

function localServer() {
    watch('src/**/*.*', buildOnly);
    return src('public')
        .pipe(webserver({
            livereload: true,
            directoryListing: false,
            open: true
        }));
}

exports.clean = cleanAll;
exports.build = build;
exports.local = series(build, localServer);
exports.default = build
